package com.sanggil.storemanager.service;

import com.sanggil.storemanager.entity.Store;
import com.sanggil.storemanager.model.StoreItem;
import com.sanggil.storemanager.repository.StoreRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StoreService {
    private final StoreRepository storeRepository;

    public void setStore(Integer age, String gender, String thing) {
        Store addData = new Store();
        addData.setAge(age);
        addData.setGender(gender);
        addData.setThing(thing);

        storeRepository.save(addData);
    }

    public List<StoreItem> getStore() {
        List<StoreItem> result = new LinkedList<>();

        List<Store> originData = storeRepository.findAll();

        for (Store item :originData) {
            StoreItem addItem = new StoreItem();
            addItem.setAge(item.getAge());
            addItem.setThing(item.getThing());

            result.add(addItem);
        }

        return result;
    }

}
