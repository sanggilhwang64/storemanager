package com.sanggil.storemanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreItem {
    private Integer age;
    private String thing;
}
