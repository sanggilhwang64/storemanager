package com.sanggil.storemanager.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class StoreRequest {
    private Integer age;
    private String gender;
    private String thing;
}
