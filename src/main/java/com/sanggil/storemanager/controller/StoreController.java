package com.sanggil.storemanager.controller;

import com.sanggil.storemanager.model.StoreItem;
import com.sanggil.storemanager.model.StoreRequest;
import com.sanggil.storemanager.service.StoreService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/store")
public class StoreController {
    private final StoreService storeService;

    @PostMapping("/data")
    public String setStore(@RequestBody StoreRequest request) {
        storeService.setStore(request.getAge(), request.getGender(), request.getThing());
        return "OK";
    }

    @GetMapping("/stores")
    public List<StoreItem> getStores() {
        List<StoreItem> result = storeService.getStore();
        return result;
    }
}
