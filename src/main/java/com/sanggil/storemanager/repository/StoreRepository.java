package com.sanggil.storemanager.repository;

import com.sanggil.storemanager.entity.Store;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StoreRepository extends JpaRepository <Store, Long> {
}
